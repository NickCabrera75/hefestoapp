﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using BRL;
using Comun;


namespace PresentacionWPF.Paginas
{
    /// <summary>
    /// Lógica de interacción para PgEditarClientes.xaml
    /// </summary>
    public partial class PgEditarClientes : Page
    {
        bool validado;
        Cliente cliente = new Cliente();
        public PgEditarClientes()
        {
            InitializeComponent();
 
        }

        private void btnAtrasMenu_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new PgClientes());
        }
        //private void CargarDatos()
        //{
        //    cliente = ClienteBrl.clienteSeleccionado;
        //    txtNombreCliente.Text = cliente.Nombre;
        //    txtPrimerApellido.Text = cliente.PrimerApellido;
        //    txtSegundoApellido.Text = cliente.SegundoApellido;
        //    txtEmailCliente.Text = cliente.Email;
        //    txtOrganizacionCliente.Text = cliente.Organizacion;
        //}

        private void btnGuardarCliente_Click(object sender, RoutedEventArgs e)
        {
        //    if (txtNombreCliente.Text != "" && txtPrimerApellido.Text != "")
        //        validado = true;
        //    else
        //        validado = false;

        //    if (validado)
        //    {
        //        cliente = new Cliente()
        //        {
        //            IdPersona = ClienteBrl.clienteSeleccionado.IdPersona,
        //            Nombre = txtNombreCliente.Text,
        //            PrimerApellido = txtPrimerApellido.Text,
        //            SegundoApellido = txtSegundoApellido.Text,
        //            Email = txtEmailCliente.Text,
        //            Organizacion = txtOrganizacionCliente.Text
        //        };
        //        ClienteBrl.Actualizar(cliente);
        //        CuadroMensaje.Visibility = Visibility.Visible;
        //        CuadroMensaje.Background = new SolidColorBrush(Colors.Green);
        //        Mensaje.Text = "DATOS INGRESADOS CORRECTAMENTE";
        //        txtNombreCliente.Text = "";
        //        txtPrimerApellido.Text = "";
        //        txtSegundoApellido.Text = "";
        //        txtEmailCliente.Text = "";
        //        txtOrganizacionCliente.Text = "";
        //        NavigationService.Navigate(new PgClientes());
        //    }
        //    else
        //    {
        //        CuadroMensaje.Background = new SolidColorBrush(Colors.Red);
        //        CuadroMensaje.Visibility = Visibility.Visible;
        //        Mensaje.Text = "ERROR AL INGRESAR LOS DATOS";
        //    }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            btnCancelar.IsEnabled = false;
            txtNombreCliente.IsEnabled = false;
            txtPrimerApellido.IsEnabled = false;
            txtSegundoApellido.IsEnabled = false;
            txtEmailCliente.IsEnabled = false;
            txtOrganizacionCliente.IsEnabled = false;
            btnGuardarCliente.IsEnabled = false;
            btnEditarCliente.IsEnabled = true;
        }

        private void btnEditarCliente_Click(object sender, RoutedEventArgs e)
        {
            btnCancelar.IsEnabled = true;
            txtNombreCliente.IsEnabled = true;
            txtPrimerApellido.IsEnabled = true;
            txtSegundoApellido.IsEnabled = true;
            txtEmailCliente.IsEnabled = true;
            txtOrganizacionCliente.IsEnabled = true;
            btnGuardarCliente.IsEnabled = true;
            btnEditarCliente.IsEnabled = false;
        }
    }
}
