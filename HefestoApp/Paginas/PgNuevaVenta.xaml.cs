﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Comun;
using BRL;

namespace PresentacionWPF.Paginas
{
    /// <summary>
    /// Lógica de interacción para PgNuevaVenta.xaml
    /// </summary>
    public partial class PgNuevaVenta : Page
    {
        public PgNuevaVenta()
        {
            InitializeComponent();
            cofigInicial();
            CarDataGrid();
            
        }

        private void BtnGuardar_Click(object sender, RoutedEventArgs e)
        {
            int count = 0;
            int[] index = new int[10];
            List<DetalleVenta> detalle = new List<DetalleVenta>();
            Venta venta = new Venta();
            txtRazonSocial.IsEnabled = true;
            dtgDetalleventa.IsEnabled = true;
            foreach (var item in dtgDetalleventa.Items)
            {
                DataGridRow row = (DataGridRow)dtgDetalleventa.ItemContainerGenerator.ContainerFromItem(item);
                detalle.Add(new DetalleVenta()
                {
                    Producto = new Producto() { IdProducto = dtgDetalleventa.Columns[0].GetCellContent(row).ToString() },
                    Descripcion = dtgDetalleventa.Columns[1].GetCellContent(row).ToString(),
                    Unidad = dtgDetalleventa.Columns[2].GetCellContent(row).ToString(),
                    Precio = decimal.Parse(dtgDetalleventa.Columns[3].GetCellContent(row).ToString()),
                    CantidadProducto = int.Parse(dtgDetalleventa.Columns[4].GetCellContent(row).ToString()),
                });
            }
            detalle.RemoveAll(x => x.Producto.IdProducto == "");
            if (txtRazonSocial.Text == "")
                MessageBox.Show("No se puede ingresar un venta sin un Cliente o Razon Social");
            if (detalle == new List<DetalleVenta>())
                MessageBox.Show("no se puede registrar la ventan sin productos");
            venta.Cliente.RazonSocial = txtRazonSocial.Text.Trim();
            venta.Empleado.IdPersona = VentasBrl.ObtenerEmpleado_idUsuario(Operaciones.idSesionIniciado);
            venta.NumeroVenta = int.Parse(txtNumVenta.Text);
            venta.Fecha = dpFecha.SelectedDate.Value.Date;
            venta.TotalVenta = decimal.Parse(txtSumTotal.Text);
            MessageBox.Show(""+ venta.Empleado.IdPersona);

            VentasBrl.InsertarVetna(venta, detalle);

        }

        private void BtnCancelar_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BtnAtras_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BtnNuevo_Click(object sender, RoutedEventArgs e)
        {

            btnGuardar.IsEnabled = true;
            btnCancelar.IsEnabled = true;
            
        }

        private void cofigInicial()
        {
            txtNumVenta.Text = VentasBrl.NuevoIdVenta() + "";
            txtRazonSocial.Text = "";
            dpFecha.Text = DateTime.Today.Date + "";
        }

        private void CarDataGrid()
        {
            List<DetalleVenta> det = new List<DetalleVenta>();
            for (int i = 0; i < 10; i++)
            {
                det.Add(new DetalleVenta()
                {
                    Producto = new Producto() { IdProducto = "" },
                    Descripcion = "",
                    Unidad = "",
                });
            }
            dtgDetalleventa.ItemsSource = null;
            dtgDetalleventa.ItemsSource = det;
        }

        private void BtnBuscarProducto_Click(object sender, RoutedEventArgs e)
        {
            Ventanas.WinListaProductos win = new Ventanas.WinListaProductos();
            win.ShowDialog();
        }
    }
}
