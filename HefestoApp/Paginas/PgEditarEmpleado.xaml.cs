﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BRL;
using Comun;

namespace PresentacionWPF.Paginas
{
    /// <summary>
    /// Lógica de interacción para PgEditarEmpleado.xaml
    /// </summary>
    public partial class PgEditarEmpleado : Page
    {
        public PgEditarEmpleado()
        {
            InitializeComponent();
            CargarDatos();
        }

        private void btnAtrasMenu_Click(object sender, RoutedEventArgs e)
        {

        }

        private void CargarDatos()
        {
            Empleado em = new Empleado();
            em = EmpleadoBrl.empleadoSeleccionado;
            txtNombre.Text = em.Nombre;
            txtPrimerApellido.Text = em.PrimerApellido;
            txtSegundoApellido.Text = em.SegundoApellido;
            //txtEmail.Text = em.Email;
            //cbxCargoEmpleado.Text = em.Rol;


            //txtUsuario.Text = em.Usuario.NombreUsuario;
            //txtContrasena.Text = em.Usuario.Contrasena;
        }
    }
}
