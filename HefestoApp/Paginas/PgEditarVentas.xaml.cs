﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Comun;
using BRL;

namespace PresentacionWPF.Paginas
{
    /// <summary>
    /// Lógica de interacción para PgEditarVentas.xaml
    /// </summary>
    public partial class PgEditarVentas : Page
    {
        Venta venta = new Venta();
        DetalleVenta detalle = new DetalleVenta();
        List<DetalleVenta> detalles = new List<DetalleVenta>();
        public PgEditarVentas()
        {
            InitializeComponent();
            this.Height = Operaciones.alto;
            this.Width = Operaciones.ancho;
            cargarDatos();
        }

        public void cargarDatos()
        {
            decimal sumTotal = 0;
            detalles.Clear();
            detalles = VentasBrl.selectDetalle();
            dtgDetalleventa.ItemsSource = null;
            dtgDetalleventa.ItemsSource = detalles;
            for (int i = 0; i < detalles.Count; i++)
            {
                sumTotal += detalles[i].Precio * detalles[i].CantidadProducto;
            }
            txtSumTotal.Text = sumTotal + "";
            txtNumVenta.Text = VentasBrl.numeroVenta+"";
            txtRazonSocial.Text= VentasBrl.RazonSocial+"";
            dpFecha.Text = VentasBrl.Fecha.Date+"";

        }

        private void BtnEliminar_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BtnEditar_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BtnAtras_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new PgVentas());
        }
    }
}
