﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using BRL;
using Comun;

namespace PresentacionWPF.Paginas
{
    public partial class PgEditarProducto : Page
    {
        Producto producto;
        bool validado = true;
        public PgEditarProducto()
        {
            InitializeComponent();
            CargaDatos();
        }

        private void btnAtrasMenu_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new PgProductos());
        }

        private void CargaDatos()
        {
            Producto producto = ProductoBrl.productoSeleccionado;
            txtCodigoProducto.Text = producto.IdProducto + "";
            txtDescripcionProducto.Text = producto.Descripcion + "";
            txtUnidadVentaProducto.Text = producto.UnidadVenta + "";
            txtPrecioProducto.Text = producto.Precio + "";
            txtCantidadProducto.Text = producto.Cantidad + "";
        }

        private void btnCancelarProducto_Click(object sender, RoutedEventArgs e)
        {
            txtCodigoProducto.Text = "";
            txtDescripcionProducto.Text = "";
            txtUnidadVentaProducto.Text = "";
            txtPrecioProducto.Text = "";
            txtCantidadProducto.Text = "";
            txtCodigoProducto.IsEnabled = false;
            txtDescripcionProducto.IsEnabled = false;
            txtUnidadVentaProducto.IsEnabled = false;
            txtPrecioProducto.IsEnabled = false;
            txtCantidadProducto.IsEnabled = false;
            btnCancelarProducto.IsEnabled = false;
            btnEditarProducto.IsEnabled = true;
        }

        private void btnEditarProducto_Click(object sender, RoutedEventArgs e)
        {
            txtCodigoProducto.IsEnabled = true;
            txtDescripcionProducto.IsEnabled = true;
            txtUnidadVentaProducto.IsEnabled = true;
            txtPrecioProducto.IsEnabled = true;
            txtCantidadProducto.IsEnabled = true;
            btnEditarProducto.IsEnabled = false;
        }

        private void btnGuardarProducto_Click(object sender, RoutedEventArgs e)
        {
            Regex validar = new Regex("0*[1-9][0-9]*");
            if (validar.IsMatch(txtCantidadProducto.Text) && validar.IsMatch(txtPrecioProducto.Text) && txtCodigoProducto.Text != "" && txtUnidadVentaProducto.Text != "" && txtDescripcionProducto.Text != "" && txtUnidadVentaProducto.Text.Length<=3)
                validado = true;
            else
                validado = false;

            if (validado)
            {
                producto = new Producto()
                {
                    IdProducto = txtCodigoProducto.Text,
                    Descripcion = txtDescripcionProducto.Text,
                    UnidadVenta = txtUnidadVentaProducto.Text,
                    Precio = decimal.Parse(txtPrecioProducto.Text),
                    Cantidad = int.Parse(txtCantidadProducto.Text)
                };
                ProductoBrl.ActualizarProducto(producto);
                CuadroMensaje.Visibility = Visibility.Visible;
                CuadroMensaje.Background = new SolidColorBrush(Colors.Green);
                Mensaje.Text = "DATOS INGRESADOS CORRECTAMENTE";
                txtCodigoProducto.Text = "";
                txtDescripcionProducto.Text = "";
                txtUnidadVentaProducto.Text = "";
                txtPrecioProducto.Text = "";
                txtCantidadProducto.Text = "";
                NavigationService.Navigate(new PgProductos());
            }
            else
            {
                CuadroMensaje.Background = new SolidColorBrush(Colors.Red);
                CuadroMensaje.Visibility = Visibility.Visible;
                Mensaje.Text = "ERROR AL INGRESAR LOS DATOS";
            }
        }

        private void txtCodigoProducto_MouseEnter(object sender, MouseEventArgs e)
        {
            CuadroMensaje.Visibility = Visibility.Collapsed;
        }
    }
}
