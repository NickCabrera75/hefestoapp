﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BRL;
using Comun;

namespace PresentacionWPF
{
    /// <summary>
    /// Lógica de interacción para PgProductos.xaml
    /// </summary>
    public partial class PgProductos : Page
    {

        public PgProductos()
        {
            InitializeComponent();
            CargarDatos();
            this.Height = Operaciones.alto;
            this.Width = Operaciones.ancho;

        }

        private void btnNuevoProducto_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Paginas.PgNuevoProducto());
        }

        private void btnEditarProducto_Click(object sender, RoutedEventArgs e)
        {
            if (dtgProductos.SelectedIndex != -1)
                NavigationService.Navigate(new Paginas.PgEditarProducto());
        }

        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dtgProductos.SelectedIndex != -1)
            {
                ProductoBrl.productoSeleccionado = this.dtgProductos.SelectedItem as Producto;
                btnEditarProducto.IsEnabled = true;
                btnEliminarProducto.IsEnabled = true;
            }
            else
            {
                btnEliminarProducto.IsEnabled = false;
                btnEditarProducto.IsEnabled = false;
            }
        }

        private void CargarDatos()
        {
            List<Producto> productos = new List<Producto>();
            productos.Clear();
            productos = ProductoBrl.GetProductos();
            this.dtgProductos.ItemsSource = null;
            this.dtgProductos.ItemsSource = productos;
        }

        private void btnEliminarProducto_Click(object sender, RoutedEventArgs e)
        {
            if (dtgProductos.SelectedIndex != -1)
            {
                PresentacionWPF.WinEliminarProducto ventanaEliminar = new WinEliminarProducto();
                ventanaEliminar.ShowDialog();
                if(ventanaEliminar.DialogResult.Value)
                {
                    
                }
                NavigationService.Navigate(new PgProductos());
            }
        }
    }
}
