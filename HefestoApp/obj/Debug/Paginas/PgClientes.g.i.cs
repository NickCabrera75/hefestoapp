﻿#pragma checksum "..\..\..\Paginas\PgClientes.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "630F5E51C8D1E375AFB1CE436D76BC42947FDD98"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Converters;
using MaterialDesignThemes.Wpf.Transitions;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace PresentacionWPF.Paginas {
    
    
    /// <summary>
    /// PgClientes
    /// </summary>
    public partial class PgClientes : System.Windows.Controls.Page, System.Windows.Markup.IComponentConnector {
        
        
        #line 22 "..\..\..\Paginas\PgClientes.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid GridBarraTitulo;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\..\Paginas\PgClientes.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid dtgClientes;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\..\Paginas\PgClientes.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn dgNombre;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\..\Paginas\PgClientes.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn dgPrimerApellido;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\..\Paginas\PgClientes.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn dgSegundoApellido;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\..\Paginas\PgClientes.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn dgEmail;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\..\Paginas\PgClientes.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn dgOrganizacion;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\..\Paginas\PgClientes.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnNuevoCliente;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\..\Paginas\PgClientes.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnEditarCliente;
        
        #line default
        #line hidden
        
        
        #line 53 "..\..\..\Paginas\PgClientes.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnEliminarCliente;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/PresentacionWPF;component/paginas/pgclientes.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Paginas\PgClientes.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.GridBarraTitulo = ((System.Windows.Controls.Grid)(target));
            return;
            case 2:
            this.dtgClientes = ((System.Windows.Controls.DataGrid)(target));
            
            #line 29 "..\..\..\Paginas\PgClientes.xaml"
            this.dtgClientes.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.dtgClientes_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 3:
            this.dgNombre = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 4:
            this.dgPrimerApellido = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 5:
            this.dgSegundoApellido = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 6:
            this.dgEmail = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 7:
            this.dgOrganizacion = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 8:
            this.btnNuevoCliente = ((System.Windows.Controls.Button)(target));
            
            #line 41 "..\..\..\Paginas\PgClientes.xaml"
            this.btnNuevoCliente.Click += new System.Windows.RoutedEventHandler(this.btnNuevoCliente_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            this.btnEditarCliente = ((System.Windows.Controls.Button)(target));
            
            #line 47 "..\..\..\Paginas\PgClientes.xaml"
            this.btnEditarCliente.Click += new System.Windows.RoutedEventHandler(this.btnEditarCliente_Click);
            
            #line default
            #line hidden
            return;
            case 10:
            this.btnEliminarCliente = ((System.Windows.Controls.Button)(target));
            
            #line 53 "..\..\..\Paginas\PgClientes.xaml"
            this.btnEliminarCliente.Click += new System.Windows.RoutedEventHandler(this.btnEliminarCliente_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

