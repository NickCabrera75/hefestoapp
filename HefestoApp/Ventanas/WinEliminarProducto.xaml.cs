﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BRL;
using Comun;

namespace PresentacionWPF
{
    /// <summary>
    /// Lógica de interacción para WinEliminarProducto.xaml
    /// </summary>
    public partial class WinEliminarProducto : Window
    {
        public WinEliminarProducto()
        {
            InitializeComponent();
            CargarMensaje();
        }

        private void btnCerrar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void CargarMensaje()
        {
            Mensaje.Text = "¿Seguro de Eliminar al Producto: " + ProductoBrl.productoSeleccionado.Descripcion + "?";
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            ProductoBrl.EliminarProducto(ProductoBrl.productoSeleccionado.IdProducto);
            this.Close();
        }
    }
}
