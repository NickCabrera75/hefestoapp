﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Comun;
using BRL;

namespace PresentacionWPF.Ventanas
{
    /// <summary>
    /// Lógica de interacción para WinListaProductos.xaml
    /// </summary>
    public partial class WinListaProductos : Window
    {
        List<Producto> productos;
        public WinListaProductos()
        {
            InitializeComponent();
            CargarDatos();
        }

        private void GridBarraTitulo_MouseDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dtgProductos.SelectedIndex != -1)
            {
                //.productoVenta = this.dtgProductos.SelectedItem as Producto;
                //    ProductoBrl.productoVenta= this.dtgProductos.SelectedItem as Producto;
                //    btnEditarProducto.IsEnabled = true;
                //    btnEliminarProducto.IsEnabled = true;
            }
            //else
            //{
            //    btnEliminarProducto.IsEnabled = false;
            //    btnEditarProducto.IsEnabled = false;
            //}
        }

        private void btnCerrar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void CargarDatos()
        { 
            productos = new List<Producto>();
            productos.Clear();
            productos = ProductoBrl.GetProductos();
            this.dtgProductos.ItemsSource = null;
            this.dtgProductos.ItemsSource = productos;
            //for (int i = 0; i < productos.Count; i++)
            //{
            //    Producto pro = productos[i];
            //    foreach (char item in pro.IdProducto)
            //    {

            //    }
                
            //}
        }

        private void BtnSeleccionar_Click(object sender, RoutedEventArgs e)
        {
            if (dtgProductos.SelectedIndex !=0)
            {
                ProductoBrl.productoVenta = this.dtgProductos.SelectedItem as Producto;
                this.Close();
            }
            else
            {
                MessageBox.Show("Tiene que seleccionar un Producto");
            }
        }

        private void TxtCodigo_TextChanged(object sender, TextChangedEventArgs e)
        {
            string cod = txtCodigo.Text;
            for (int i = 0; i < productos.Count; i++)
            {
                Producto pro = productos[i];
                foreach (char item in pro.IdProducto)
                {
                    //for (int i = 0; i < ; i++)
                    //{

                    //}
                }

            }
        }
    }
}
