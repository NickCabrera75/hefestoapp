﻿using System.Windows;
using System.Windows.Input;
using BRL;
using Comun;


namespace PresentacionWPF
{
    /// <summary>
    /// Lógica de interacción para Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        Usuario user = new Usuario();
        public Login()
        {
            InitializeComponent();
        }

        private void btnCerrar_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void obtenermedidas_Click(object sender, RoutedEventArgs e)
        {
        }

        private void btnMinimizar_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void lblResetPassword_MouseDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void BtnSesion_Click(object sender, RoutedEventArgs e)
        {
            //user = UsuarioBrl.Login(txtUser.Text, txtPassword.Password.ToString());
            if (!(user == new Usuario())) 
            {
                //Operaciones.idSesionIniciado = user.IdUsuario;
                MainWindow menu = new MainWindow();
                menu.Show();
                this.Close();
            }
        }
    }
}
