﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comun;

namespace DAL
{
    /// <summary>
    /// Clase encargada de establecer el formato de cada log e insertarlo
    /// </summary>
    public class Logs
    {
        /// <summary>
        /// Registra cualquier crud realizado con exito en una tabla
        /// </summary>
        /// <param name="accion"></param>
        /// <param name="nombreTabla"></param>
        public static void escribirAccion(string accion, string nombreTabla)
        {

            System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} realizado con exito en la tabla {2}. Usuario: {3}", DateTime.Now, accion, nombreTabla, Sesion.nombreUsuarioSesion));
        }
        /// <summary>
        /// Registra cualquier crud realizado con exito en muchas tablas
        /// </summary>
        /// <param name="accion"></param>
        /// <param name="nombreTabla"></param>
        /// <param name="plural"></param>
        public static void escribirAccion(string accion, string nombreTabla, bool plural)
        {
            System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} realizado con exito en las tablas {2}. Usuario: {3}", DateTime.Now, accion, nombreTabla, Sesion.nombreUsuarioSesion));
        }

        /// <summary>
        /// Registra un crud fallido en una tabla
        /// </summary>
        /// <param name="accion"></param>
        /// <param name="nombreTabla"></param>
        /// <param name="mensaje"></param>
        public static void registrarError(string accion, string nombreTabla, string mensaje)
        {
            System.Diagnostics.Debug.WriteLine(string.Format("{0} Error: {1} fallido en la tabla {2}. Usuario: {3}. Detalle: {4}", DateTime.Now, accion, nombreTabla, Sesion.nombreUsuarioSesion, mensaje));

        }

        /// <summary>
        /// Registra un crud fallido en muchas tablas
        /// </summary>
        /// <param name="accion"></param>
        /// <param name="nombreTabla"></param>
        /// <param name="mensaje"></param>
        internal static void registrarError(string accion, string nombreTabla, string mensaje, bool plural)
        {
            System.Diagnostics.Debug.WriteLine(string.Format("{0} Error: {1} fallido en las tabla {2}. Usuario: {3}. Detalle: {4}", DateTime.Now, accion, nombreTabla, Sesion.nombreUsuarioSesion, mensaje));

        }
    }
}
