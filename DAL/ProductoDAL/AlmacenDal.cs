﻿using System;
using System.Collections.Generic;
using Comun;
using System.Data.SqlClient;

namespace DAL
{
    class AlmacenDal
    {
        static Almacen almacen = new Almacen();
        static List<Almacen> Almacenes = new List<Almacen>();
        static SqlCommand comando = null;
        static SqlDataReader reader = null;

        public static void Insertar(Almacen almacen)
        {
            string queryString = @"INSERT INTO Almacen(idAlmacen, Descripcion, Estado, Direccion) VALUES(@idProducto, @Descripcion, @Estado, @Direccion)";
            SqlConnection conexion = Comun.OperacionesSql.ObtenerConexion();
            SqlTransaction transaccion = null;
            try
            {
                conexion.Open();
                transaccion = conexion.BeginTransaction();
                comando = Comun.OperacionesSql.CreateBasicCommandWithTransaction(queryString, transaccion, conexion);
                comando.Parameters.AddWithValue("@idProducto", almacen.IdAlmacen);
                comando.Parameters.AddWithValue("@Descripcion", almacen.Descripcion);
                comando.Parameters.AddWithValue("@Estado", 1);
                comando.Parameters.AddWithValue("@Direccion", almacen.Direccion);
                OperacionesSql.ExecuteBasicCommandWithTransaction(comando);

                transaccion.Commit();
            }
            catch (SqlException ex)
            {
                
                throw ex;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
            finally
            {
                conexion.Close();
            }

        }

        public static Almacen GetProducto(int id)
        {
            string queryString = @"SELECT * FROM Producto WHERE idProducto = @idProducto";
            try
            {
                comando = OperacionesSql.CreateBasicCommand(queryString);
                comando.Parameters.AddWithValue("@idProducto", id);
                reader = OperacionesSql.ExecuteDataReaderCommand(comando);
                while (reader.Read())
                {
                    almacen.IdAlmacen = reader.GetInt16(0);
                    almacen.Descripcion = reader.GetString(1);
                    almacen.Direccion = reader.GetString(2);

                }

            }
            catch (Exception ex)
            {
                
                throw ex;
            }

            return almacen;
        }

        public static List<Almacen> GetProductos()
        {
            string queryString = @"SELECT * FROM Almacen WHERE Estado=1";
            try
            {
                comando = OperacionesSql.CreateBasicCommand(queryString);
                reader = OperacionesSql.ExecuteDataReaderCommand(comando);
                while (reader.Read())
                {
                    Almacenes.Add(new Almacen
                    {
                        IdAlmacen = reader.GetInt16(0),
                        Descripcion = reader.GetString(1),
                        Direccion =reader.GetString(2)
                    });
                }
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
            return Almacenes;
        }

        public static void Eliminar(string id)
        {
            
            string queryString = @"UPDATE Producto SET Estado = 0 WHERE idProducto = @idProducto";
            try
            {
                comando = OperacionesSql.CreateBasicCommand(queryString);
                comando.Parameters.AddWithValue("@idProducto", id);
                OperacionesSql.ExecuteBasicCommand(comando);
            }
            catch (SqlException ex)
            {
                
                throw ex;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
            
        }

        public static void Actualizar(Producto producto)
        {
            
            string queryString = @"UPDATE Producto SET Descripcion = @Descripcion, UnidadVenta = @UnidadVenta, Precio = @Precio, Cantidad = @Cantidad WHERE idProducto=@idProducto";
            try
            {
                comando = OperacionesSql.CreateBasicCommand(queryString);
                comando.Parameters.AddWithValue("@Descripcion", producto.Descripcion);
                comando.Parameters.AddWithValue("@UnidadVenta", producto.UnidadVenta);
                comando.Parameters.AddWithValue("@Precio", producto.Precio);
                comando.Parameters.AddWithValue("@Cantidad", producto.Cantidad);
                comando.Parameters.AddWithValue("@idProducto", producto.IdProducto);
                OperacionesSql.ExecuteBasicCommand(comando);
            }
            catch (SqlException ex)
            {
                
                throw ex;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
            
        }
    }
}
