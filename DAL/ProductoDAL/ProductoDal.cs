﻿using System;
using System.Collections.Generic;
using Comun;
using System.Data.SqlClient;

namespace DAL
{
    public static class ProductoDal
    {
        static Producto producto = new Producto();
        static SqlCommand comando = null;
        static SqlDataReader reader = null;

        public static void Insertar(Producto producto)
        {
            string queryString = @"INSERT INTO Producto(idProducto, Descripcion, UnidadVenta, Precio, Cantidad, Eliminado) VALUES(@idProducto, @Descripcion, @UnidadVenta, @Precio, @Cantidad, @Eliminado)";
            SqlConnection conexion = OperacionesSql.ObtenerConexion();
            SqlTransaction transaccion = null;
            try
            {
                conexion.Open();
                transaccion = conexion.BeginTransaction();
                comando = OperacionesSql.CreateBasicCommandWithTransaction(queryString, transaccion, conexion);
                comando.Parameters.AddWithValue("@idProducto", producto.IdProducto);
                comando.Parameters.AddWithValue("@Descripcion", producto.Descripcion);
                comando.Parameters.AddWithValue("@UnidadVenta", producto.UnidadVenta);
                comando.Parameters.AddWithValue("@Precio", producto.Precio);
                comando.Parameters.AddWithValue("@Cantidad", producto.Cantidad);
                comando.Parameters.AddWithValue("@Eliminado", 0);
                OperacionesSql.ExecuteBasicCommandWithTransaction(comando);
                transaccion.Commit();
            }
            catch (SqlException ex)
            {throw ex;}
            catch (Exception ex)
            {throw ex;}
            finally
            {
                conexion.Close();
            }
            
        }

        public static Producto GetProducto(string id)
        {
            string queryString = @"SELECT * FROM Producto WHERE idProducto = @idProducto";
            try
            {
                comando = OperacionesSql.CreateBasicCommand(queryString);
                comando.Parameters.AddWithValue("@idProducto", id);
                reader = OperacionesSql.ExecuteDataReaderCommand(comando);
                while (reader.Read())
                {
                    producto.IdProducto = reader.GetString(0);
                    producto.Descripcion = reader.GetString(1);
                    producto.UnidadVenta = reader.GetString(2);
                    producto.Precio = reader.GetDecimal(3);
                    producto.Cantidad = reader.GetInt16(4);
                }
            }
            catch (Exception ex)
            {throw ex;}

            return producto;
        }

        public static List<Producto> GetProductos()
        {
            List<Producto> lista = new List<Producto>();
            string queryString = @"SELECT * FROM Producto WHERE Eliminado=0";
            try
            {
                comando = OperacionesSql.CreateBasicCommand(queryString);
                reader = OperacionesSql.ExecuteDataReaderCommand(comando);
                while (reader.Read())
                {
                    lista.Add(new Producto
                    {
                        IdProducto = reader.GetString(0),
                        Descripcion = reader.GetString(1),
                        UnidadVenta = reader.GetString(2),
                        Precio = reader.GetDecimal(3),
                        Cantidad = reader.GetInt16(4)
                    });
                }
            }
            catch (Exception ex)
            {throw ex;}
            return lista;
        }

        public static void Eliminar(string id)
        {
            
            string queryString = @"UPDATE Producto SET Estado = 0 WHERE idProducto = @idProducto";
            try
            {
                comando = OperacionesSql.CreateBasicCommand(queryString);
                comando.Parameters.AddWithValue("@idProducto", id);
                OperacionesSql.ExecuteBasicCommand(comando);
            }
            catch (SqlException ex)
            {throw ex;}
            catch (Exception ex)
            {throw ex;}
            
        }

        public static void Actualizar(Producto producto)
        {
            string queryString = @"UPDATE Producto SET Descripcion = @Descripcion, UnidadVenta = @UnidadVenta, Precio = @Precio, Cantidad = @Cantidad WHERE idProducto=@idProducto";
            try
            {
                comando = OperacionesSql.CreateBasicCommand(queryString);
                comando.Parameters.AddWithValue("@Descripcion", producto.Descripcion);
                comando.Parameters.AddWithValue("@UnidadVenta", producto.UnidadVenta);
                comando.Parameters.AddWithValue("@Precio", producto.Precio);
                comando.Parameters.AddWithValue("@Cantidad", producto.Cantidad);
                comando.Parameters.AddWithValue("@idProducto", producto.IdProducto);
                OperacionesSql.ExecuteBasicCommand(comando);
            }
            catch (SqlException ex)
            {
                
                throw ex;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
            
        }

        //public static void InsertarConTransaccion(Producto persona, SqlTransaction transaccion, SqlConnection conexion)
        //{
        //    Operaciones.WriteLogsDebug("PersonaDal", "Insertar", string.Format("{0} Info: {1}",
        //    DateTime.Now.ToString(), "Empezando a ejecutar el metodo acceso a datos para crear un persona"));

        //    SqlCommand command = null;

        //    //Consulta para insertar personas
        //    string queryString = @"INSERT INTO Persona(IdPersona, Nombre, PrimerApellido, SegundoApellido, IdUsuario, IdTelefono, Email, Estado) 
        //                            VALUES(@idPersona, @nombre, @primerApellido, @segundoApellido, @idUsuario, @idTelefono, @email, @estado)";
        //    try
        //    {
        //        command = OperacionesSql.CreateBasicCommandWithTransaction(queryString, transaccion, conexion);
        //        command.Parameters.AddWithValue("@idPersona", persona.IdPersona);
        //        command.Parameters.AddWithValue("@nombre", persona.Nombre);
        //        command.Parameters.AddWithValue("@primerApellido", persona.PrimerApellido);
        //        command.Parameters.AddWithValue("@segundoApellido", persona.SegundoApellido);
        //        command.Parameters.AddWithValue("@idUsuario", persona.Usuario);
        //        command.Parameters.AddWithValue("@idTelefono", persona.Telefono);
        //        command.Parameters.AddWithValue("@email", persona.Email);
        //        command.Parameters.AddWithValue("@estado", false);
                //OperacionesSql.ExecuteBasicCommandWithTransaction(command);
        //    }
        //    catch (SqlException ex)
        //    {
        //        Operaciones.WriteLogsRelease("PersonaDal", "Insertar", string.Format("{0} {1} Error: {2}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
        //        throw ex;
        //    }
        //    catch (Exception ex)
        //    {
        //        Operaciones.WriteLogsRelease("PersonaDal", "Insertar", string.Format("{0} {1} Error: {2}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
        //        throw ex;
        //    }
        //    Operaciones.WriteLogsDebug("PersonaDal", "Insertar", string.Format("{0} {1} Info: {2}", DateTime.Now.ToString(), DateTime.Now.ToString(), "Termino de ejecutar  el metodo acceso a datos para insertar persona"));
        //}

        //public static void EliminarConTransaccion(Guid idPersona, SqlTransaction transaccion, SqlConnection conexion)
        //{
        //    Operaciones.WriteLogsDebug("PersonaDal", "Eliminar", string.Format("{0} Info: {1}", DateTime.Now.ToString(), "Empezando a ejecutar el metodo acceso a datos para eliminar un Persona"));
        //    SqlCommand command = null;
        //    // Proporcionar la cadena de consulta 
        //    string queryString = @"UPDATE Persona SET Eliminado=0
        //                            WHERE IdPersona = @idPersona";
        //    try
        //    {
        //        command = OperacionesSql.CreateBasicCommandWithTransaction(queryString, transaccion, conexion);
        //        command.Parameters.AddWithValue("@idPersona", idPersona);
        //        OperacionesSql.ExecuteBasicCommandWithTransaction(command);
        //        //elimina al usuario
        //        UsuarioDal.EliminarPorIdPersonaConTransaccion(idPersona, transaccion, conexion);
        //    }
        //    catch (SqlException ex)
        //    {
        //        Operaciones.WriteLogsRelease("PersonaDal", "Eliminar", string.Format("{0} Error: {1}", DateTime.Now.ToString(), ex.Message));
        //        throw ex;
        //    }
        //}

    }
}
