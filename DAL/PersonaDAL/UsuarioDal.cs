﻿using System;
using System.Data.SqlClient;
using Comun;
using System.Data;
using System.Collections.Generic;

namespace DAL
{

    /// <summary>
    /// Clase Usuario perteneciente a la capa de datos
    /// </summary>
    public sealed class UsuarioDal : AbstractDAL
    {
        #region Atributos, Propiedades y Constructores

        private Usuario usuario;
        public Usuario Usuario
        {
            get
            {
                return usuario;
            }

            set
            {
                usuario = value;
            }
        }


        public UsuarioDal()
        {

        }
        public UsuarioDal(Usuario usuario)
        {
            this.usuario = usuario;
        }
        #endregion

        #region Metodos

        /// <summary>
        /// Realiza un eliminado logico de Usuario desde Persona
        /// </summary>
        public override void Delete()
        {
            string query = "UPDATE Persona SET estado = 0, fUpdate = CURRENT_TIMESTAMP WHERE idPersona = :idUsuario";
            try
            {
                SqlCommand cmd = OperacionesSql.CreateBasicCommand(query);
                cmd.Parameters.Add("idUsuario", usuario.IdPersona);
                OperacionesSql.ExecuteBasicCommand(cmd);
                Logs.escribirAccion("UPDATE", "Persona");
            }
            catch (Exception ex)
            {
                Logs.registrarError("UPDATE", "Persona", ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// Obtiene un usuario a partir de su id
        /// </summary>
        /// <param name="idUsuario"></param>
        /// <returns></returns>
        public Usuario Get(int idUsuario)
        {
            Usuario res = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string query = "SELECT U.idUsuario, P.nombres, P.primerApellido, P.segundoApellido, U.nombreUsuario, U.rol, U.latitud, U.longitud, U.foto FROM Usuario U INNER JOIN Persona P ON P.idPersona = U.idUsuario WHERE U.idUsuario = :idUsuario";
            try
            {
                cmd = OperacionesSql.CreateBasicCommand(query);
                cmd.Parameters.Add("idUsuario", idUsuario);
                dr = OperacionesSql.ExecuteDataReaderCommand(cmd);

                while (dr.Read())
                {
                    res = new Usuario(int.Parse(dr[0].ToString()), dr[1].ToString(), dr[2].ToString(), dr[3].ToString(), dr[4].ToString(), dr[5].ToString(), byte.Parse(dr[8].ToString()));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return res;
        }

        /// <summary>
        /// Verifica si un usuario ya esta registrado
        /// </summary>
        /// <returns></returns>
        public bool verSiExiste()
        {
            DataTable res = new DataTable();
            string query = "SELECT * FROM Usuario U INNER JOIN Persona P ON P.idPersona = U.idUsuario WHERE U.nombreUsuario = :nombreUsuario AND P.estado = 1";
            try
            {
                SqlCommand cmd = OperacionesSql.CreateBasicCommand(query);
                cmd.Parameters.Add("nombreUsuario", usuario.NombreUsuario);
                res = OperacionesSql.ExecuteDataTableCommand(cmd);
                return res.Rows.Count > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Verifica si hay un usuario existente igual al actual
        /// </summary>
        /// <returns></returns>
        public bool VerSiHayOtro()
        {
            DataTable res = new DataTable();
            string query = "SELECT * FROM Usuario WHERE idUsuario <> :idUsuario AND nombreUsuario = :nombreUsuario";
            try
            {
                SqlCommand cmd = OperacionesSql.CreateBasicCommand(query);
                cmd.Parameters.Add("idUsuario", usuario.IdPersona);
                cmd.Parameters.Add("nombreUsuario", usuario.NombreUsuario);
                res = OperacionesSql.ExecuteDataTableCommand(cmd);
                return res.Rows.Count > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Inserta un Usuario
        /// </summary>
        public override void Insert()
        {
            string query1 = "INSERT INTO Persona(idPersona, nombres, primerApellido, segundoApellido, idUsuario) VALUES (:idPersona, :nombres, :primerApellido, :segundoApellido, :idSesion)";
            string query2 = @"INSERT INTO Usuario(idUsuario, nombreUsuario, password, rol, latitud, longitud, foto) 
                            VALUES (:idUsuario, :nombreUsuario, dbms_obfuscation_toolkit.md5( input_string => :password), :rol, :latitud, :longitud, :foto)";

            List<SqlCommand> cmds = new List<SqlCommand>();
            try
            {
                cmds = OperacionesSql.CreateNBasicCommand(2);

                SqlCommand cmd1 = cmds[0];
                SqlCommand cmd2 = cmds[1];

                cmd1.CommandText = query1;
                cmd2.CommandText = query2;

                int idPersona = OperacionesSql.GetIDGenerateTable("PERSONA");

                cmd1.Parameters.AddWithValue("idPersona", idPersona);
                cmd1.Parameters.AddWithValue("nombre", usuario.Nombre);
                cmd1.Parameters.AddWithValue("primerApellido", usuario.PrimerApellido);
                cmd1.Parameters.AddWithValue("segundoApellido", usuario.SegundoApellido);
                cmd1.Parameters.AddWithValue("idSesion", Sesion.idSesion);

                //int idPersona = Methods.GetIDGenerateTable("Persona");

                cmd2.Parameters.AddWithValue("idUsuario", idPersona);
                cmd2.Parameters.AddWithValue("nombreUsuario", usuario.NombreUsuario);
                cmd2.Parameters.AddWithValue("contraseña", usuario.Contraseña);
                cmd2.Parameters.AddWithValue("rol", usuario.Rol);
                cmd2.Parameters.AddWithValue("foto", usuario.Foto);

                OperacionesSql.ExecuteMultipleBasicCommand(cmds);

                Logs.escribirAccion("INSERT", "Persona, Usuario", true);

            }
            catch (Exception ex)
            {
                Logs.registrarError("INSERT", "Persona, Usuario", ex.Message, true);
                throw ex;
            }
        }

        /// <summary>
        /// Selecciona todos los usuarios
        /// </summary>
        /// <returns></returns>
        public override DataTable Select()
        {
            DataTable res = new DataTable();
            string query = "SELECT * FROM vwUsuarioSelect ORDER BY 2";
            try
            {
                SqlCommand cmd = OperacionesSql.CreateBasicCommand(query);
                res = OperacionesSql.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return res;
        }
        //Crear vistas

        /// <summary>
        /// Actualiza un usuario
        /// </summary>
        public override void Update()
        {
            string query1 = "UPDATE Persona SET nombres = :nombres, primerApellido = :primerApellido, segundoApellido = :segundoApellido, fUpdate = CURRENT_TIMESTAMP, idUsuario = :idSesion WHERE idPersona = :idUsuario";
            string query2 = "UPDATE Usuario SET nombreUsuario = :nombreUsuario, rol = :rol, latitud = :latitud, longitud = :longitud, foto = :foto WHERE idUsuario = :idUsuario";

            List<SqlCommand> cmds = new List<SqlCommand>();
            try
            {
                cmds = OperacionesSql.CreateNBasicCommand(2);

                SqlCommand cmd1 = cmds[0];
                SqlCommand cmd2 = cmds[1];

                cmd1.CommandText = query1;
                cmd2.CommandText = query2;

                cmd1.Parameters.AddWithValue("nombre", usuario.Nombre);
                cmd1.Parameters.AddWithValue("primerApellido", usuario.PrimerApellido);
                cmd1.Parameters.AddWithValue("segundoApellido", usuario.SegundoApellido);
                cmd1.Parameters.AddWithValue("idSesion", Sesion.idSesion);
                cmd1.Parameters.AddWithValue("idUsuario", usuario.IdPersona);

                cmd2.Parameters.AddWithValue("nombreUsuario", usuario.NombreUsuario);
                cmd2.Parameters.AddWithValue("rol", usuario.Rol);
                cmd2.Parameters.AddWithValue("foto", usuario.Foto);
                cmd2.Parameters.AddWithValue("idUsuario", usuario.IdPersona);

                OperacionesSql.ExecuteMultipleBasicCommand(cmds);

                Logs.escribirAccion("UPDATE", "Persona, Usuario", true);

            }
            catch (Exception ex)
            {
                Logs.registrarError("UPDATE", "Persona, Usuario", ex.Message, true);
                throw ex;
            }
        }

        /// <summary>
        /// Retorna un usuario a partir de su login y password
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public DataTable Login(string usuario, string password)
        {
            DataTable dt = new DataTable();
            string query = "SELECT U.idUsuario, (P.nombre||' '||P.primerApellido), U.rol FROM Usuario U INNER JOIN Persona P ON P.idPersona = U.idUsuario WHERE P.estado = 1 AND U.nombreUsuario = :nombreUsuario AND U.password = dbms_obfuscation_toolkit.md5( input_string => :password)";
            try
            {
                SqlCommand cmd = OperacionesSql.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("nombreUsuario", usuario);
                cmd.Parameters.AddWithValue("password", password);
                dt = OperacionesSql.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        /// <summary>
        /// Cambia la contraseña
        /// </summary>
        /// <param name="contraseñaNueva"></param>
        public void UpdateContraseña(string contraseñaNueva)
        {
            string query1 = "UPDATE Usuario SET password = dbms_obfuscation_toolkit.md5( input_string => :password) WHERE idUsuario = :idUsuario";
            string query2 = "UPDATE Persona SET fUpdate = CURRENT_TIMESTAMP WHERE idPersona = :idUsuario";
            List<SqlCommand> cmds = new List<SqlCommand>();
            try
            {

                cmds = OperacionesSql.CreateNBasicCommand(2);

                SqlCommand cmd1 = cmds[0];
                SqlCommand cmd2 = cmds[1];

                cmd1.CommandText = query1;
                cmd2.CommandText = query2;

                cmd1.Parameters.AddWithValue("contraseña", contraseñaNueva);
                cmd1.Parameters.AddWithValue("idUsuario", Sesion.idSesion);
                cmd2.Parameters.AddWithValue("idUsuario", Sesion.idSesion);

                OperacionesSql.ExecuteMultipleBasicCommand(cmds);
                Logs.escribirAccion("UPDATE PASSWORD", "Persona, Usuario", true);
            }
            catch (Exception ex)
            {
                Logs.registrarError("UPDATE PASSWORD", "Persona, Usuario", ex.Message, true);
                throw ex;
            }
        }

        /// <summary>
        /// Reestablece el password de una forma aleatoria
        /// </summary>
        public void ReestablecerPassword()
        {
            string query1 = "UPDATE Usuario SET password = dbms_obfuscation_toolkit.md5( input_string => :password) WHERE idUsuario = :idUsuario";
            string query2 = "UPDATE Persona SET fUpdate = CURRENT_TIMESTAMP WHERE idPersona = :idUsuario";
            List<SqlCommand> cmds = new List<SqlCommand>();
            try
            {

                cmds = OperacionesSql.CreateNBasicCommand(2);
                SqlCommand cmd1 = cmds[0];
                SqlCommand cmd2 = cmds[1];
                cmd1.CommandText = query1;
                cmd2.CommandText = query2;


                cmd1.Parameters.AddWithValue("contraseña", usuario.Contraseña);//.OracleDbType = OracleDbType.Varchar2;
                cmd1.Parameters.AddWithValue("idUsuario", usuario.IdPersona);
                cmd2.Parameters.AddWithValue("idUsuario", usuario.IdPersona);
                OperacionesSql.ExecuteMultipleBasicCommand(cmds);
                Logs.escribirAccion("UPDATE PASSWORD", "Persona, Usuario", true);
            }
            catch (Exception ex)
            {
                Logs.registrarError("UPDATE PASSWORD", "Persona, Usuario", ex.Message, true);
                throw ex;
            }
        }
        #endregion
    }
}