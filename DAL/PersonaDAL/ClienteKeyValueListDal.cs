﻿using System;
using System.Data.SqlClient;
using Comun;

namespace DAL
{
    public static class ClienteKeyValueListDal
    {
        /// <summary>
        /// Retorna una lista de identifificadores y nombre completo de empleados
        /// </summary>
        /// <param name="apellido">Primer Apellido  de empleados</param>
        /// <returns></returns>
        public static ClienteKeyValueList Obtener(string apellido)
        {
            ClienteKeyValueList lista = new ClienteKeyValueList();
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string query = @"Select c.IdCliente, CONCAT(p.Nombre, ' ', p.PrimerApellido, ' ' ,p.SegundoApellido) as NombreCompleto
                            from Empleado e inner join Persona p on e.IdCliente = p.IdPersona
                            Where e.Eliminado = 0 and p.PrimerApellido like @apellido";
            try
            {
                cmd = OperacionesSql.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@apellido", string.Format("%{0}%", apellido));
                dr = OperacionesSql.ExecuteDataReaderCommand(cmd);
                while (dr.Read())
                {
                    lista.Add(new EmpleadoKeyValue()
                    {
                        IdPersona = dr.GetGuid(0),
                        NombreCompleto = dr.GetString(1)
                    });
                }
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("ClienteDal", "Obtener(Get)", string.Format("{0} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return lista;
        }
    }
}
