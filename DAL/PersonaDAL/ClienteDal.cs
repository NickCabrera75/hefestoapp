﻿using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using Comun;

namespace DAL
{
    public static class ClienteDal
    {
        /// <summary>
        /// Inserta un Cliente a la base de datos 
        /// </summary>
        /// <param name="cliente"></param>
        public static void Insertar(Cliente cliente)
        {
            Operaciones.WriteLogsDebug("CLienteDal", "Insertar", string.Format("{0} Info: {1}",
            DateTime.Now.ToString(), "Empezando a ejecutar el metodo acceso a datos para crear un Cliente"));

            SqlCommand command = null;

            //Consulta para insertar cliente 
            string queryString = @"INSERT INTO Cliente(IdPersona, Organizacion, Eliminado) 
                                    VALUES(@idUsuario, @organizacion, @eliminado)";
            //Declaro e inicio la conexion
            SqlConnection conexion = OperacionesSql.ObtenerConexion();
            //Declaro la transaccion
            SqlTransaction transaccion = null;
            try
            {
                //Abro la conexion a la base de datos
                conexion.Open();

                //Inicio la transaccion
                transaccion = conexion.BeginTransaction();

                //Inserto a la persona
                //PersonaDal.InsertarConTransaccion(cliente as Cliente, transaccion, conexion);

                ////Inserto al empleado
                //command = OperacionesSql.CreateBasicCommandWithTransaction(queryString, transaccion, conexion);
                //command.Parameters.AddWithValue("@idUsuario", cliente.IdCliente);
                //command.Parameters.AddWithValue("@organizacion", cliente.RazonSocial);
                //command.Parameters.AddWithValue("@eliminado", false);
                //OperacionesSql.ExecuteBasicCommandWithTransaction(command);

                transaccion.Commit();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para obtener  un cliente
        /// </summary>
        /// <param name="id">Identificado del cliente </param>
        /// <returns>Cliente</returns>
        public static Cliente Obtener(int id)
        {
            Cliente cliente = new Cliente();
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string query = @"SELECT Razonsocial, Eliminado FROM Cliente WHERE IdCliente=@id ";
            try
            {
                cmd = OperacionesSql.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@id", id);
                dr = OperacionesSql.ExecuteDataReaderCommand(cmd);
                //cliente = PersonaDal.ObtenerCliente(id);
                while (dr.Read())
                {
                    cliente.RazonSocial = dr.GetString(0);
                    cliente.Eliminado = dr.GetBoolean(1);
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return cliente;
        }

        public static List<Cliente> ObtenerClientes()
        {
            List<Cliente> clientes = new List<Cliente>();
            Cliente cliente = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string query = @"SELECT IdPersona, Organizacion, Eliminado FROM Cliente WHERE Eliminado=0";
            try
            {
                cmd = OperacionesSql.CreateBasicCommand(query);
                dr = OperacionesSql.ExecuteDataReaderCommand(cmd);
                while (dr.Read())
                {
                    cliente = new Cliente();
                    //cliente = PersonaDal.ObtenerCliente(dr.GetGuid(0));
                    cliente.RazonSocial = dr.GetString(1);
                    cliente.Eliminado = dr.GetBoolean(2);
                    clientes.Add(cliente);
                }
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("CLienteDal", "ObtenerClientes", string.Format("{0} {1} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("CLienteDal", "ObtenerClientes", string.Format("{0} {1} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return clientes;
        }

        /// <summary>
        /// Método para actulizar a un cliente
        /// </summary>
        /// <param name="cliente"></param>
        public static void Actualizar(Cliente cliente)
        {
            Operaciones.WriteLogsDebug("CLienteDal", "Actualizar", string.Format("{0} Info: {1}", DateTime.Now.ToString(), "Empezando a ejecutar el metodo acceso a datos para Actualizar un Cliente"));

            SqlCommand command = null;

            // Proporcionar la cadena de consulta 
            string queryString = @"UPDATE Cliente SET Organizacion=@organizacion WHERE IdPersona=@idPersona";
            try
            {

                command = OperacionesSql.CreateBasicCommand(queryString);
                command.Parameters.AddWithValue("@organizacion", cliente.RazonSocial);
                command.Parameters.AddWithValue("@idPersona", cliente.IdCliente);

                //Actualizo a la persona
                //PersonaDal.Actualizar(cliente as Persona);

                OperacionesSql.ExecuteBasicCommand(command);
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("CLienteDal", "Actualizar", string.Format("{0} Error: {1}", DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("CLienteDal", "Actualizar", string.Format("{0} Error: {1}", DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

            Operaciones.WriteLogsDebug("CLienteDal", "Actualizar", string.Format("{0}  Info: {1}", DateTime.Now.ToString(), "Termino de ejecutar  el metodo acceso a datos para Actualizar un Cliente"));
        }

        /// <summary>
        /// Método para eliminar a un cliente
        /// </summary>
        /// <param name="cliente"></param>
        public static void Eliminar(Guid idCliente)
        {
            Operaciones.WriteLogsDebug("CLienteDal", "Eliminar", string.Format("{0} Info: {1}", DateTime.Now.ToString(), "Empezando a ejecutar el metodo acceso a datos para eliminar un Cliente"));

            SqlCommand command = null;

            // Proporcionar la cadena de consulta 
            string queryString = @"UPDATE Cliente SET Eliminado=1
                                    WHERE IdPersona=@id";
            //Declaro e inicio la conexion
            SqlConnection conexion = OperacionesSql.ObtenerConexion();

            //Declaro la transaccion
            SqlTransaction transaccion = null;
            try
            {
                //Abro la conexion a la base de datos
                conexion.Open();

                //Inicio la transaccion
                transaccion = conexion.BeginTransaction();

                command = OperacionesSql.CreateBasicCommandWithTransaction(queryString, transaccion, conexion);
                command.Parameters.AddWithValue("@id", idCliente);

                //Elimino a la persona
                //PersonaDal.EliminarConTransaccion(idCliente, transaccion, conexion);

                OperacionesSql.ExecuteBasicCommandWithTransaction(command);

                transaccion.Commit();
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("CLienteDal", "Eliminar", string.Format("{0} Error: {1}", DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("CLienteDal", "Eliminar", string.Format("{0} Error: {1}", DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

            Operaciones.WriteLogsDebug("CLienteDal", "Eliminar", string.Format("{0}  Info: {1}", DateTime.Now.ToString(), "Termino de ejecutar  el metodo acceso a datos para Eliminar un Cliente"));
        }
    }

}
