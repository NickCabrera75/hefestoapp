﻿using Comun;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace DAL
{
    public class VentasDal
    {

        //INSERT
        public static void Insert(Venta objeto)
        {
            string query = "INSERT INTO Venta(fecha,totalVenta,NumeroVenta,idEmpleado,idCliente,eliminado) VALUES (@fecha,@totalVenta,@numeroVetna,@idEmpleado,@idCliente,@eliminado)";
            try
            {
                SqlCommand cmd = OperacionesSql.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@fecha", objeto.Fecha);
                cmd.Parameters.AddWithValue("@totalVenta", objeto.TotalVenta);
                cmd.Parameters.AddWithValue("@idEmpleado", objeto.Empleado.IdPersona);
                cmd.Parameters.AddWithValue("@numeroVenta", objeto.NumeroVenta );
                cmd.Parameters.AddWithValue("@idCliente", objeto.Cliente.IdCliente);
                cmd.Parameters.AddWithValue("@eliminado", 0);
                OperacionesSql.ExecuteBasicCommand(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //SELECT
        public static List<Venta> Select()
        {
            List<Venta> res = new List<Venta>();
            string query = "SELECT NumeroVenta,fecha,TotalVenta,idCliente,Eliminado,idVenta FROM Venta";
            try
            {
                SqlCommand cmd = OperacionesSql.CreateBasicCommand(query);
                SqlDataReader reader = OperacionesSql.ExecuteDataReaderCommand(cmd);
                while (reader.Read())
                {
                    Venta v = new Venta();
                    Cliente c = new Cliente();
                    v.NumeroVenta = reader.GetInt32(0);
                    if (!string.IsNullOrEmpty(reader.GetValue(1).ToString()))
                    {
                        v.Fecha = reader.GetDateTime(1);
                    }
                    v.TotalVenta = reader.GetDecimal(2);
                    v.Cliente = new Cliente() { IdCliente = reader.GetInt32(3) };
                    v.Estado = reader.GetByte(4);
                    v.VentaAux = reader.GetInt32(5);
                    res.Add(v);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return res;
        }
        //UPDATE
        public static void Update(Venta objeto)
        {
            string query = "UPDATE Venta SET fecha=@fecha,totalVenta=@totalVenta,estado=@estado WHERE idVenta= @idVenta";
            try
            {
                SqlCommand cmd = OperacionesSql.CreateBasicCommand(query);

                cmd.Parameters.AddWithValue("@fecha", objeto.Fecha);
                cmd.Parameters.AddWithValue("@totalVenta", objeto.TotalVenta);
                cmd.Parameters.AddWithValue("@estado", objeto.Estado);
                cmd.Parameters.AddWithValue("@idVenta", objeto.IdVenta);
                OperacionesSql.ExecuteBasicCommand(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //GET
        public static Venta Get(Guid idVenta)
        {
            Venta res = null;
            string query = @"SELECT idVenta,fecha,totalVenta,idUsuario,idCliente,estado FROM Venta WHERE idVenta=@idVenta";
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                cmd = OperacionesSql.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@idVenta", idVenta);
                dr = OperacionesSql.ExecuteDataReaderCommand(cmd);
                while (dr.Read())
                {
                    res = new Venta();
                    res.IdVenta = int.Parse(dr[0].ToString());
                    if (!String.IsNullOrEmpty(dr[1].ToString()))
                    {
                        res.Fecha = DateTime.Parse(dr[1].ToString());
                    }
                    res.TotalVenta = int.Parse(dr[2].ToString());
                    res.Empleado.IdPersona = dr.GetInt32(3);
                    res.Cliente.IdCliente = dr.GetInt32(4);
                    res.Estado = byte.Parse(dr[5].ToString());

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
                dr.Close();
            }
            return res;
        }
        //Soft Delete
        public static void SoftDelete(Guid idVenta)
        {
            string query = "UPDATE Venta SET estado=0 WHERE idVenta= @idVenta";
            try
            {
                SqlCommand cmd = OperacionesSql.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@idVenta", idVenta);
                OperacionesSql.ExecuteBasicCommand(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static List<DetalleVenta> GetDetalles(int id)
        {
            List<DetalleVenta> res = new List<DetalleVenta>();
            string query = "SELECT idProducto,CantidadProducto,Precio FROM DetalleVenta where idVenta=@id";
            try
            {
                SqlCommand cmd = OperacionesSql.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@id", id);
                SqlDataReader reader = OperacionesSql.ExecuteDataReaderCommand(cmd);
                while (reader.Read())
                {
                    DetalleVenta d = new DetalleVenta();
                    d.Producto = new Producto() { IdProducto = reader.GetString(0) };
                    d.CantidadProducto = reader.GetInt16(1);
                    d.Precio = reader.GetDecimal(2);
                    res.Add(d);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return res;
        }

        public static int NuevoIdVenta()
        {
            int nuevoid = 0;
            try
            {
                string query = "Select top (1) NumeroVenta FROM Venta order by NumeroVenta desc ";
                SqlCommand cmd = OperacionesSql.CreateBasicCommand(query);
                SqlDataReader reader = OperacionesSql.ExecuteDataReaderCommand(cmd);
                while (reader.Read())
                {
                    nuevoid = reader.GetInt32(0);
                }
                return nuevoid;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

    }
}