CREATE TABLE Usuario( 
idUsuario int primary key identity,
NombreUsuario varchar(40) not null,
Contrasena varchar(40) not null,
Eliminado bit not null)

CREATE TABLE Persona( 
idPersona int primary key identity, 
Nombre varchar(40) not null,
PrimerApellido varchar(40) not null,
SegundoApellido varchar(40),
IdUsuario uniqueidentifier,
Email varchar(35),
Eliminado bit not null,)
--foreign key (idUsuario) references Usuario(idUsuario))

CREATE TABLE Telefono( 
idTelefono int primary key identity,
Numero int not null,
idPersona int not null,
Eliminado bit not null,
foreign key (idPersona) references Persona(idPersona))

CREATE TABLE Almacen( 
idAlmacen tinyint primary key,
Descripcion varchar(60),
Direccion varchar(150) not null,
Eliminado bit not null)

CREATE TABLE Producto( 
idProducto VARCHAR(10) primary key, 
Descripcion VARCHAR(30) not null, --TUBERIA DE AGUA   --MANGUERA 
UnidadVenta VARCHAR(3) not null, -- POR METROS   -- POR BARRA  -- POR ROLLO
Precio DECIMAL(18,2) not null,
Cantidad SMALLINT not null,
Eliminado TINYINT not null)
--foreign key (idAlmacen) references Almacen(idAlmacen))

CREATE TABLE Cliente(
idCliente int primary key not null,
RazonSocial varchar(60) not null,
Nit int,
Eliminado bit not null)
--foreign key(idUsuario) references Usuario(idUsuario),
--foreign key(idCliente) references Persona(idPersona))

CREATE TABLE Proveedor(
idProveedor int primary key not null,
RazonSocial varchar(60) not null,
Nit int,
Eliminado bit not null)
--foreign key (idUsuario) references Usuario(idUsuario),
--foreign key(idProveedor) references Persona(idPersona))

--CREATE TABLE Rol(
--idRol tinyint primary key,
--Descripcion varchar(25)not null)

CREATE TABLE Empleado(
idEmpleado int primary key not null,
Rol varchar(20) not null,
Eliminado bit not null,
idUsuario int,
foreign key (idEmpleado) references Persona(idPersona),
foreign key (idUsuario) references Usuario(idUsuario))
--foreign key (idRol) references Rol(idRol))

--foreign key (idTipoUsuario) references TipoUsuario(idTipoUsuario),
--foreign key(idUsuario) references Persona(idPersona))

CREATE TABLE Venta(
idVenta INT PRIMARY KEY,
idCliente int not null,
idEmpleado int not null,
NumeroVenta int not null,
Fecha DATETIME not null,
TotalVenta DECIMAL(18,2) not null,
Estado tinyint not null, -- 20 6  --25 7   -0 eliminado -1 vendido -2 anulado 
FOREIGN KEY (idCliente) REFERENCES Cliente(idCliente),
foreign key (idEmpleado) references Empleado(idEmpleado))

CREATE TABLE DetalleVenta(
idDetalleVenta int primary key,
idVenta int not null,
idProducto varchar(10) not null,
CantidadProducto smallint not null,
foreign key (idProducto) references Producto(idProducto),
foreign key (idVenta) references Venta(idVenta))

create table Compra(
idCompra int primary key,
idProveedor int not null,
idEmpleado int not null,
Fecha date not null,
TotalCompra decimal(18,2) not null,
Estado tinyint not null,
foreign key(idEmpleado) references Empleado(idEmpleado),
foreign key (idProveedor) references Proveedor(idProveedor))

CREATE TABLE DetalleCompra(
idDetalleCompra int primary key not null,
idCompra int not null,
idProducto varchar(10) not null,
CantidadProducto smallint not null,
foreign key (idProducto) references Producto(idProducto),
foreign key (idCompra) references Compra(idCompra))

CREATE TABLE Auditoria(
idVenta int,
idEmpleado int,
Accion varchar(40) not null)
--foreign key (idVenta) references Venta(idVenta),
--foreign key (idEmpleado) references Empleado(idEmpleado))

CREATE TABLE ImagenesProducto(
idImagen int primary key,
idProducto varchar(10)not null,
Direccion varchar,
foreign key (idProducto) references Producto(idProducto))

CREATE Table AlmacenProducto(
idAlmacen tinyint not null,
idProducto varchar(10) not null,
Cantidad int not null,
foreign key (idProducto) references Producto(idProducto),
foreign key (idAlmacen) references Almacen(idAlmacen))

--CREATE TABLE ProductoAlmacen(
--idProductoAlmacen VARCHAR(10) primary key,
--Descripcion varchar(30) not null,
--Unidad VARCHAR(3) not null,
--Precio DECIMAL not null,
--Cantidad SMALLINT not null, --500 -50  +50   450
--idAlmacen tinyint,
--foreign key (idAlmacen) references Almacen(idAlmacen))

--CREATE TABLE TrasladoProducto(
--idTrasladoProducto int primary key,
--idProductoAlmacen varchar(10),
--Cantidad int not null,
--idProducto varchar(10),
--foreign key (idProducto) references Producto(idProducto),
--foreign key (idProductoAlmacen) references ProductoAlmacen(idProductoAlmacen))

--Create procedure PA_SiguienteIdPersonas()
--as 
--begin
--Begin transaction
--Declare @error int
--set @error =0
--if object_id('Personas') in not null
--	begin
--	Select MAX(idPersona)
--	From Persona
--insert into Usuario(idUsuario,NombreUsuario,Contrasena,Eliminado) values('b6423155-ff88-4ba2-9498-34e269a188da','default','default',0)
