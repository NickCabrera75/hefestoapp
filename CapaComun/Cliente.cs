﻿using System;

namespace Comun
{
    /// <summary>
    /// Clase que sirve para crear objetos Cliente
    /// </summary>
    public class Cliente
    {
        #region propiedades

        public int IdCliente { get; set; }
        public string RazonSocial { get; set; }
        public int Nit { get; set; }
        public bool Eliminado { get; set; }

        #endregion
        public Cliente() { } 
    }

}
