﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace Univalle.Fie.Sistemas.BaseDeDatos2.SevpaApp.Comun
{
    public class MetodosConsultas
    {
        private static string connectionString = "Data Source=localhost;Initial Catalog=BDD2;Integrated Security=True";

        public static SqlCommand CrearComandoBasico(string query)
        {
            return new SqlCommand(query, new SqlConnection(connectionString));
        }

        public static void EjecutarComandoBasico(SqlCommand cmd)
        {
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }

        public static DataTable EjecutarTablaDeDatos(SqlCommand cmd)
        {
            DataTable dt = new DataTable();
            try
            {
                cmd.Connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return dt;
        }

        public static SqlCommand RellenarComando(SqlCommand cmd, string[] parametros, ArrayList valores)
        {
            for (int i = 0; i < parametros.Length; i++)
            {
                cmd.Parameters.AddWithValue("@" + parametros[i], valores[i]);
            }
            return cmd;
        }

        public static void TransaccionSimple(SqlCommand cmd1, SqlCommand cmd2)
        {
            SqlTransaction tran = null;
            try
            {
                cmd2.Connection = cmd1.Connection;
                cmd1.Connection.Open();
                tran = cmd1.Connection.BeginTransaction();
                cmd1.Transaction = tran;
                cmd1.ExecuteNonQuery();

                cmd2.Transaction = tran;
                cmd2.ExecuteNonQuery();

                tran.Commit();
            }
            catch (Exception ex)
            {
                tran.Rollback();
                throw ex;
            }
            finally
            {
                cmd1.Connection.Close();
            }
        }

        public static void TransaccionMultiple(SqlCommand cmd, List<Tuple<SqlCommand, SqlCommand>> list)
        {
            SqlTransaction trans = null;
            try
            {
                list.ForEach(delegate (Tuple<SqlCommand, SqlCommand> item) { item.Item1.Connection = cmd.Connection; item.Item2.Connection = cmd.Connection; });
                cmd.Connection.Open();
                trans = cmd.Connection.BeginTransaction();
                cmd.Transaction = trans;
                cmd.ExecuteNonQuery();

                foreach (Tuple<SqlCommand, SqlCommand> item in list)
                {
                    item.Item1.Transaction = trans;
                    item.Item1.ExecuteNonQuery();
                    item.Item2.Transaction = trans;
                    item.Item2.ExecuteNonQuery();
                }
                trans.Commit();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }

        public static string EjecutarComandoEscalar(SqlCommand cmd)
        {
            string res = "";
            try
            {
                cmd.Connection.Open();
                res = cmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return res;
        }

        public static int ObtenerIdSiguiente(string tabla)
        {
            int res = -1;
            string query = "SELECT IDENT_CURRENT('" + tabla + "') + IDENT_INCR('" + tabla + "')";
            try
            {
                SqlCommand cmd = CrearComandoBasico(query);
                res = int.Parse(EjecutarComandoEscalar(cmd));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return res;
        }

        public static SqlCommand CreateBasicCommand()
        {
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            return cmd;
        }

        public static SqlCommand CreateBasicCommand(string query)
        {
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(query);
            cmd.Connection = connection;
            return cmd;
        }

        public static List<SqlCommand> CreateNBasicCommand(int n)
        {
            List<SqlCommand> res = new List<SqlCommand>();

            //Creamos la conexion
            SqlConnection connection = new SqlConnection(connectionString);

            for (int i = 0; i < n; i++)
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = connection;
                res.Add(cmd);
            }


            return res;
        }

        #region Ejecucion de SqlCommand

        public static void ExecuteBasicCommand(SqlCommand cmd)
        {
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }


        }

        public static void ExecuteBasicCommand(SqlCommand cmd, string query)
        {
            try
            {
                cmd.CommandText = query;
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }


        }

        public static void ExecuteNBasicCommand(List<SqlCommand> lista)
        {
            SqlTransaction tran = null;

            try
            {
                lista[0].Connection.Open();
                tran = lista[0].Connection.BeginTransaction();
                lista[0].Transaction = tran;
                for (int i = 0; i < lista.Count; i++)
                {
                    lista[i].Transaction = tran;
                    lista[i].ExecuteNonQuery();
                }



                //NO existe error
                tran.Commit();

            }
            catch (Exception ex)
            {
                tran.Rollback();
                throw ex;
            }
            finally
            {
                lista[0].Connection.Close();
            }
        }

        public static int GetIDGenerateTable(string tabla)
        {
            int res = -1;
            string query = "SELECT IDENT_CURRENT('" + tabla + "')+IDENT_INCR('" + tabla + "')";

            try
            {
                SqlCommand cmd = CreateBasicCommand(query);
                res = int.Parse(ExecuteScalarCommand(cmd));
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return res;
        }

        public static string ExecuteScalarCommand(SqlCommand cmd)
        {
            try
            {
                cmd.Connection.Open();
                return cmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }


        }

        public static void Execute2BasicCommand(SqlCommand cmd1, SqlCommand cmd2)
        {
            SqlTransaction tran = null;

            try
            {
                cmd1.Connection.Open();
                tran = cmd1.Connection.BeginTransaction();
                cmd1.Transaction = tran;
                cmd1.ExecuteNonQuery();

                cmd2.Transaction = tran;
                cmd2.ExecuteNonQuery();

                //NO existe error
                tran.Commit();

            }
            catch (Exception ex)
            {
                tran.Rollback();
                throw ex;
            }
            finally
            {
                cmd1.Connection.Close();
            }
        }

        public static DataTable ExecuteDataTableCommand(SqlCommand cmd)
        {
            DataTable res = new DataTable();
            try
            {
                cmd.Connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(res);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return res;
        }

        public static SqlDataReader ExecuteDataReaderCommand(SqlCommand cmd)
        {
            SqlDataReader res = null;
            try
            {
                cmd.Connection.Open();
                res = cmd.ExecuteReader();
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return res;
        }

        #endregion
    }
}
