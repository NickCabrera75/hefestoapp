﻿using System;

namespace Comun
{
    /// <summary>
    /// Clase que sirve para crear objetos Cliente
    /// </summary>
    public class Cliente : Persona
    {
        #region propiedades

        /// <summary>
        /// Campo donde se guarda el nombre del cliente
        /// </summary>
        public string Organizacion { get; set; }

        /// <summary>
        /// Sirve para guardar el Nit del cliente
        /// </summary>
        public int Nit { get; set; }
        #endregion
        public Cliente() { } 
    }

}
