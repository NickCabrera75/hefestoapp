﻿using System;

namespace Comun
{
    /// <summary>
    /// Clase que sirve para crear usuario
    /// </summary>
    public class Usuario
    {
        public static int Usuariosesion;
        public static Guid defaultIdUsuario = Guid.Parse("b6423155-ff88-4ba2-9498-34e269a188da");
        public static string defaultNombreUsuario = "default";
        public static string defaultContrasena = "default";
        #region propiedades

        /// <summary>
        /// Identificador del usuario
        /// </summary>
        public Guid IdUsuario { get; set; }

        /// <summary>
        /// Nombre de usuario
        /// </summary>
        public string NombreUsuario { get; set; }

        /// <summary>
        /// Contraseña del usuario
        /// </summary>
        public string Contrasena { get; set; }

        /// <summary>
        /// Eliminado que sirve para el eliminado lògico
        /// </summary>
        public bool Eliminado { get; set; }

        #endregion
        public Usuario() {  }
    }
}
