﻿using System;

namespace Comun
{
    /// <summary>
    /// Clase que sirve para crear Venta
    /// </summary>
    public class Venta
    {
        #region propiedades

        /// <summary>
        /// Identificador de la venta
        /// </summary>
        public int IdVenta { get; set; }

        /// <summary>
        /// Sirve para identificar a que Cliente se realizo la venta
        /// </summary>
        public Cliente Cliente { get; set; }

        /// <summary>
        /// Sirve para identificar que empleado realizo dicha venta
        /// </summary>
        public Empleado Empleado { get; set; }

        /// <summary>
        /// Fecha sirve para guardar la fecha en la que se realizo la venta
        /// </summary>
        public DateTime Fecha { get; set; }

        /// <summary>
        /// TotalVenta Sirve para el monto total vendido
        /// </summary>
        public decimal TotalVenta { get; set; }
        
        /// <summary>
        /// Sirve para Revisar el Estado de una venta
        /// </summary>
        public byte Estado { get; set; }

        #endregion
        public Venta() { }
    }
}
