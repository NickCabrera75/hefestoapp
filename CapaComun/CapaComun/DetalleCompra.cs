﻿using System;

namespace Comun
{
    /// <summary>
    /// Clase que sirve para crear objetos DetalleCompra
    /// </summary>
    public class DetalleCompra
    {
        #region propiedades

        /// <summary>
        /// 
        /// </summary>
        public int IdDetalleCompra { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Compra Compra { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Producto Producto { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int CantidadProducto { get; set; }

        #endregion
        public DetalleCompra() { }
    }
}
