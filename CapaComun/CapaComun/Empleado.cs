﻿    
using System;

namespace Comun
{
    /// <summary>
    /// Clase hija que sirve para crear empleados
    /// </summary>
    public class Empleado : Persona
    {
        #region propiedades
        /// <summary>
        /// Cargo del empleado
        /// </summary>
        public string Cargo { get; set; }

        /// <summary>
        /// Eliminado que sirve para el eliminado lògico
        /// </summary>
        public bool Eliminado { get; set; }

        /// <summary>
        /// Usuario que sirve para guardar a usuario de la persona
        /// </summary>
        public Usuario Usuario { get; set; }
        #endregion
        public Empleado() { }
    }
}
