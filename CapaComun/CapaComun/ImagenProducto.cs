﻿using System;

namespace Comun
{
    public class ImagenProducto
    {
        #region propiedades

        /// <summary>
        /// Identificador de la imagen del producto
        /// </summary>
        public int IdImagen { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Producto Producto { get; set; }

        /// <summary>
        /// Direccion donde se encuentra la imagen
        /// </summary>
        public string Direccion { get; set; }

        #endregion
        public ImagenProducto() { }
    }
}
