﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comun
{
    public class Localizacion
    {
        #region propiedades

        public int IdLocalizacion { get; set; }
        public decimal Latitud { get; set; }
        public decimal Longitud { get; set; }
        public bool Eliminado { get; set; }
        #endregion
    }
}
