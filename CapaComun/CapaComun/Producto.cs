﻿using System;

namespace Comun
{
    /// <summary>
    /// Clase que sirve para crear objetos Producto
    /// </summary>
    public class Producto
    {
         #region propiedades

        /// <summary>
        /// Identificador del producto
        /// </summary>
        public string IdProducto { get; set; }

        /// <summary>
        /// Observaciones del producto
        /// </summary>
        public string Descripcion { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string UnidadVenta { get; set; }

        /// <summary>
        /// Precio del producto
        /// </summary>
        public decimal Precio { get; set; }

        /// <summary>
        /// Cantidad que existe del producto
        /// </summary>
        public int Cantidad { get; set; }

        /// <summary>
        /// Eliminado logico
        /// </summary>
        public byte Eliminado { get; set; }

        #endregion
        public Producto() { }
    }
}
