﻿using System;

namespace Comun
{
    /// <summary>
    /// Clase que sirve para crear objetos Proveedor
    /// </summary>
    public class Proveedor : Persona
    {
        #region propiedades

        /// <summary>
        /// 
        /// </summary>
        public string Organizacion { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Nit { get; set; }

        #endregion
        public Proveedor() { }
    }
}
