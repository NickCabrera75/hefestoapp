﻿using System;

namespace Comun
{
    public class AlmacenProducto
    {
        #region propiedades

        public int IdAlmacenProducto { get; set; }
        public Almacen Almacen { get; set; }
        public Producto Producto { get; set; }
        public int Cantidad { get; set; }

        #endregion
        public AlmacenProducto() { }
    }
}
