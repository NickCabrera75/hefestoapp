﻿using System;

namespace Comun
{
    /// <summary>
    /// Clase que sirve para crear objetos Registro
    /// </summary>
    public class Registro
    {
        #region propiedades

        public int IdRegistro { get; set; }
        public Venta Venta { get; set; }
        public Empleado Empleado { get; set; }
        public string Accion { get; set; }
        
        #endregion
        public Registro() { }
    }
}
