﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comun
{
    public class Validates
    {
        public static string CambiarComaPorPunto(string cadena)
        {
            string nueva = string.Empty;
            for (int i = 0; i < cadena.Length; i++)
            {
                if (cadena[i] != ',')
                    nueva += cadena[i];
                else
                    nueva += '.';
            }
            return nueva;
        }
        public static bool OnlyLettersAndSpaces(string cad)
        {
            for (int i = 0; i < cad.Length; i++)
            {
                if (!Char.IsLetter(cad[i]) && cad[i] != ' ')
                {
                    return false;
                }

            }
            return true;
        }
        public static bool OnlyNumbers(string cad)
        {
            for (int i = 0; i < cad.Length; i++)
            {
                if (!Char.IsDigit(cad[i]))
                {
                    return false;
                }

            }
            return true;
        }
        public static bool OnlyNumbersAndComa(string cad)
        {
            byte contador = 0;
            for (int i = 0; i < cad.Length; i++)
            {
                if (!Char.IsDigit(cad[i]) && cad[i] != ',')
                {
                    return false;
                }
                if (cad[i] == ',')
                {
                    contador++;
                    if (contador > 2)
                        return false;
                }

            }
            return true;
        }
    }
}
