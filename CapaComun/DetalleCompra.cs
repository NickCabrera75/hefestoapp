﻿using System;

namespace Comun
{
    /// <summary>
    /// Clase que sirve para crear objetos DetalleCompra
    /// </summary>
    public class DetalleCompra
    {
        #region propiedades

        public int IdDetalleCompra { get; set; }
        public Compra Compra { get; set; }
        public string IdProducto { get; set; }
        public int CantidadProducto { get; set; }

        #endregion
        public DetalleCompra() { }
    }
}
