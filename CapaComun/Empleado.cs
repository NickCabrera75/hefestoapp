﻿    
using System;

namespace Comun
{
    /// <summary>
    /// Clase hija que sirve para crear empleados
    /// </summary>
    public class Empleado : Persona
    {
        public Empleado() { }
        private string ci;
        private string cargo;

        public string Ci
        {
            get
            {
                return ci;
            }
            set
            {
                ci = value;
            }
        }
        public string Cargo
        {
            get
            {
                return cargo;
            }
            set
            {
                cargo = value;
            }
        }
    }
}
