﻿using System;

namespace Comun
{
    /// <summary>
    /// Clase que sirve para crear objetos Compra
    /// </summary>
    public class Compra
    {
        #region propiedades

        public int IdCompra { get; set; }
        public Proveedor Proveedor { get; set; }
        public Empleado Empleado { get; set; }
        public DateTime Fecha { get; set; }
        public int TotalCompra { get; set; }
        public byte Estado { get; set; }

        #endregion
        public Compra() { }
    }
}
