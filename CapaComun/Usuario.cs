﻿using System;
using System.Security.Cryptography;
using System.IO;

namespace Comun
{
    /// <summary>
    /// Clase que sirve para crear objetos usuario
    /// </summary>
    public class Usuario : Persona
    {
        public static int Usuariosesion;
        public static Guid defaultIdUsuario = Guid.Parse("b6423155-ff88-4ba2-9498-34e269a188da");
        public static string defaultNombreUsuario = "default";
        public static string defaultContrasena = "default";

        #region Atributos
        private string nombreUsuario;
        private string contraseña;
        private string rol;
        private byte foto;
        #endregion

        #region propiedades

        public string NombreUsuario
        {
            get { return nombreUsuario; }
            set { nombreUsuario = value; }
        }

        public string Contraseña
        {
            get { return contraseña; }
            set { contraseña = value; }
        }

        public string Rol
        {
            get { return rol; }
            set { rol = value; }
        }

        public byte Foto
        {
            get
            {
                return foto;
            }

            set
            {
                foto = value;
            }
        }
        #endregion

        #region Constructores
        public Usuario() { }

        public Usuario(int idPersona, string nombre, string primerApellido, string segundoApellido, string nombreUsuario, string rol, byte foto)
        {
            IdPersona = idPersona;
            Nombre = nombre;
            PrimerApellido = primerApellido;
            SegundoApellido = segundoApellido;
            Estado = 1;
            this.NombreUsuario = nombreUsuario;
            this.Contraseña = null;
            this.Rol = rol;
            this.foto = foto;
        }
        public Usuario(string nombre, string primerApellido, string segundoApellido, string nombreUsuario, string rol, byte foto)
        {
            Nombre = nombre;
            PrimerApellido = primerApellido;
            SegundoApellido = segundoApellido;
            Estado = 1;
            this.nombreUsuario = nombreUsuario;
            this.Contraseña = null;
            this.rol = rol;
            this.foto = foto;
        }
        #endregion

        #region Metodos
        public string GenerarNombreUsuario()
        {
            Random r = new Random();
            return Nombre.Substring(0, 2) + PrimerApellido.Substring(0, 2) + r.Next(0, 9) + r.Next(0, 9) + r.Next(0, 9);
        }
        public string GenerarPassword()
        {
            Random r = new Random();

            string original = nombreUsuario + r.Next(100, 999) + Rol + DateTime.Now.ToString("hh:mm:ss");

            // C Crea una nueva instancia de la clase TripleDESCryptoServiceProvider
            // Esto genera una nueva clave (key) y vector de inicializacion(IV)
            using (TripleDESCryptoServiceProvider myTripleDES = new TripleDESCryptoServiceProvider())
            {
                // Encripta la cadena en un arreglo de bytes.
                byte[] encrypted = EncryptStringToBytes(original, myTripleDES.Key, myTripleDES.IV);

                string stgEnc = BitConverter.ToString(encrypted);
                stgEnc = QuitarCaracter(stgEnc, '-');
                return stgEnc.Substring(r.Next(0, (stgEnc.Length - 5)), 5);
            }
        }

        private string QuitarCaracter(string cadena, char caracter)
        {
            string nueva = string.Empty;
            for (int i = 0; i < cadena.Length; i++)
            {
                if (cadena[i] != caracter)
                    nueva += cadena[i];
            }
            return nueva;
        }

        static byte[] EncryptStringToBytes(string plainText, byte[] Key, byte[] IV)
        {
            // Revisa los argumentos
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("Key");
            byte[] encrypted;
            // Crea un objeto TripleDESCryptoServiceProvider
            // Con la llave especifica y vector (key and IV).
            using (TripleDESCryptoServiceProvider tdsAlg = new TripleDESCryptoServiceProvider())
            {
                tdsAlg.Key = Key;
                tdsAlg.IV = IV;

                // Crea un escriptador para la ejecucion del stream transform.
                ICryptoTransform encryptor = tdsAlg.CreateEncryptor(tdsAlg.Key, tdsAlg.IV);

                // Crea los streams usados para encriptacion.
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            //Escribe toda la informacion en el stream
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }

            // Devuelve los bytes encriptados del stream de memoria
            return encrypted;
        }
        #endregion
    }
}
