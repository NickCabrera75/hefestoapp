﻿using System;

namespace Comun
{
    /// <summary>
    /// Clase que sirve para crear objetos Venta
    /// </summary>
    public class Venta
    {
        #region propiedades

        public int IdVenta { get; set; }
        public Cliente Cliente { get; set; }
        public Empleado Empleado { get; set; }
        public DateTime Fecha { get; set; }
        public decimal TotalVenta { get; set; }
        public int NumeroVenta { get; set; }
        public byte Estado { get; set; }
        public string EstadoAux { get; set; }
        public string ClienteAux { get; set; }
        public int VentaAux { get; set; }
        public string EmpleadoAux { get; set; }


        #endregion
        public Venta() { }
    }
}
