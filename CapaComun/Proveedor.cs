﻿using System;

namespace Comun
{
    /// <summary>
    /// Clase que sirve para crear objetos Proveedor
    /// </summary>
    public class Proveedor : Persona
    {
        #region propiedades

        public string Organizacion { get; set; }


        #endregion
        public Proveedor() { }
    }
}
