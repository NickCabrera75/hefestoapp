﻿using System;

namespace Comun
{
    /// <summary>
    /// Clase que sirve para crear objetos DetalleVenta
    /// </summary>
    public class DetalleVenta
    {
        #region propiedades

        public int IdDetalleVenta { get; set; }
        public Venta Venta { get; set; }
        public Producto Producto { get; set; }
        public int CantidadProducto { get; set; }
        public decimal Precio { get; set; }
        public string ProductoAux { get; set; }
        public int VentaAux { get; set; }
        public string Descripcion { get; set; }
        public string Unidad { get; set; }
        public decimal Total { get; set; }

        #endregion
        public DetalleVenta() { }
    }
}
