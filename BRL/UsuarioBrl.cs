﻿using System;
using System.Data.SqlClient;
using Comun;
using DAL;
using System.Data;

namespace BRL
{
    /// <summary>
    /// Clase para manejar la lógica de negocio del usuario
    /// </summary>
    public class UsuarioBRL : AbstractBRL
    {
        #region Atributos
        private UsuarioDal dal;
        #endregion

        #region Constructores
        public UsuarioBRL()
        {

        }
        public UsuarioBRL(Usuario usuario)
        {
            dal = new UsuarioDal(usuario);
        }
        #endregion

        #region Propiedades
        public UsuarioDal Dal
        {
            get
            {
                return dal;
            }

            set
            {
                dal = value;
            }
        }
        #endregion

        #region Metodos
        public override void Delete()
        {
            dal.Delete();
        }

        public override void Insert()
        {
            dal.Insert();
        }

        public override DataTable Select()
        {
            dal = new UsuarioDal();
            return dal.Select();
        }

        public override void Update()
        {
            dal.Update();
        }

        public Usuario Get(int idUsuario)
        {
            dal = new UsuarioDal();
            return dal.Get(idUsuario);
        }

        public DataTable Login(string usuario, string password)
        {
            dal = new UsuarioDal();
            return dal.Login(usuario, password);
        }

        public bool VerSiExiste()
        {
            return dal.verSiExiste();
        }

        public bool VerSiHayOtro()
        {
            return dal.VerSiHayOtro();
        }
        public void ReestablecerPassword()
        {
            dal.ReestablecerPassword();
        }

        public void UpdateContraseña(string contraseñaNueva)
        {
            dal = new UsuarioDal();
            dal.UpdateContraseña(contraseñaNueva);
        }
        #endregion
    }
}
