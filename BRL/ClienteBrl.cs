﻿using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using Comun;
using DAL;

namespace BRL
{
    /// <summary>
    /// Clase para manejar la lógica de negocio del empleado
    /// </summary>
    public static class ClienteBrl
    {
        public static Cliente clienteSeleccionado = new Cliente();
        /// <summary>
        /// Método lógica de negocio para insertar un cliente
        /// </summary>
        /// <param name="cliente"></param>
        public static void Insertar(Cliente cliente)
        {
            Operaciones.WriteLogsDebug("ClienteBrl", "Insertar", string.Format("{0} Info: {1}",
            DateTime.Now.ToString(),
            "Empezando a ejecutar el método lógica de negocio para crear un cliente"));

            try
            {
                ClienteDal.Insertar(cliente);
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("ClienteBrl", "Insertar", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("ClienteBrl", "Insertar", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

            Operaciones.WriteLogsDebug("ClienteBrl", "Insertar", string.Format("{0} Info: {1}",
                DateTime.Now.ToString(),
                "Termino de ejecutar  el método lógica de negocio para insertar empleado"));
        }

        /// <summary>
        /// método lógica de negocio para actulizar un cliente
        /// </summary>
        /// <param name="cliente"></param>
        public static void Actualizar(Cliente cliente)
        {
            Operaciones.WriteLogsDebug("ClienteBrl", "Actualizar", string.Format("{0} Info: {1}",
            DateTime.Now.ToString(),
            "Empezando a ejecutar el método lógica de negocio para actualizar un cliente"));

            try
            {
                DAL.ClienteDal.Actualizar(cliente);
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("ClienteBrl", "Actualizar", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("ClienteBrl", "Actualizar", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

            Operaciones.WriteLogsDebug("ClienteBrl", "Actualizar", string.Format("{0} Info: {1}",
                DateTime.Now.ToString(), DateTime.Now.ToString(),
                "Termino de ejecutar  el método lógica de negocio para actualizar cliente"));
        }

        /// <summary>
        /// método lógica de negocio para eliminar un cliente
        /// </summary>
        /// <param name="cliente"></param>
        public static void Eliminar(Guid id)
        {
            Operaciones.WriteLogsDebug("ClienteBrl", "Eliminar", string.Format("{0} Info: {1}",
            DateTime.Now.ToString(),
            "Empezando a ejecutar el método lógica de negocio para Eliminar un cliente"));

            try
            {
                DAL.ClienteDal.Eliminar(id);
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("ClienteBrl", "Eliminar", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("ClienteBrl", "Eliminar", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

            Operaciones.WriteLogsDebug("ClienteBrl", "Eliminar", string.Format("{0} Info: {1}",
                DateTime.Now.ToString(), DateTime.Now.ToString(),
                "Termino de ejecutar  el método lógica de negocio para Eliminar cliente"));
        }

        /// <summary>
        /// método lógica de negocio para eliminar un cliente
        /// </summary>
        /// <param name="cliente"></param>
        public static Cliente Obtener(int id)
        {
            Operaciones.WriteLogsDebug("ClienteBrl", "Obtener", string.Format("{0} Info: {1}",
            DateTime.Now.ToString(),
            "Empezando a ejecutar el método lógica de negocio para Obtener un cliente"));

            try
            {
                return DAL.ClienteDal.Obtener(id);
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("ClienteBrl", "Obtener", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("ClienteBrl", "Obtener", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
        }

        public static List<Cliente> GetClientes()
        {
            List<Cliente> clientes = null;
            try
            {
                clientes = DAL.ClienteDal.ObtenerClientes();
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("ClienteBrl", "GetClientes", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("ClienteBrl", "GetClientes", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            return clientes;
        }


    }
}
