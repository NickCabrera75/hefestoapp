﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Comun;
using DAL;
using System.Data;


namespace BRL
{
    /// <summary>
    /// Clase para manejar la lógica de negocio de la empleado
    /// </summary>
    public class EmpleadoBrl
    {
        public static Empleado empleadoSeleccionado;
        public static DataTable Select()
        {
            return EmpleadoDal.Select();
        }
        public static void Insert(Empleado emp)
        {
            EmpleadoDal.Insert(emp);
        }

        public static void Update(Empleado emp)
        {
            EmpleadoDal.Update(emp);
        }
        public static void Delete(int idEmpleado)
        {
            EmpleadoDal.Delete(idEmpleado);
        }

        public static Empleado Get(int idEmpleado)
        {
            return EmpleadoDal.Get(idEmpleado);
        }
    }
}
