﻿using System;
using System.Data.SqlClient;
using Comun;
using DAL;

namespace BRL
{
    public class ClienteKeyValueListBrl
    {
        public static ClienteKeyValueList Obtener(string apellido)
        {
            Operaciones.WriteLogsDebug("ClienteKeyValueListBrl", "Obtener", string.Format("{0} Info: {1}",
             DateTime.Now.ToString(),
             "Empezando a ejecutar el método lógica de negocio para Obtener un ClienteKeyValueList"));

            try
            {
                return ClienteKeyValueListDal.Obtener(apellido);
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("ClienteKeyValueListBrl", "Obtener", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("ClienteKeyValueListBrl", "Obtener", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
        }
    }
}
