﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comun;
using DAL;

namespace BRL
{
    public static class VentasBrl
    {
        public static string RazonSocial;
        public static int numeroVenta = 0;
        public static DateTime Fecha;
        public static int ventaSeleccionado = 0;
        static Venta venta = null;
        static List<Venta> ventas = null;
        static List<DetalleVenta> detalles = new List<DetalleVenta>();

        public static List<Venta> select()
        {
            ventas=VentasDal.Select();
            for (int i = 0; i < ventas.Count; i++)
            {
                ventas[i].ClienteAux = ClienteDal.Obtener(ventas[i].Cliente.IdCliente).RazonSocial;
                ventas[i].Fecha = ventas[i].Fecha.Date;
                if (ventas[i].Estado == 0)
                    ventas[i].EstadoAux = "Valido";
                else
                    ventas[i].EstadoAux = "Anulado";
            }
            return ventas;
        }

        public static List<DetalleVenta> selectDetalle()
        {
            detalles = VentasDal.GetDetalles(ventaSeleccionado);
            for (int i = 0; i < detalles.Count; i++)
            {
                detalles[i].Producto=ProductoDal.GetProducto(detalles[i].Producto.IdProducto);
                detalles[i].Unidad = detalles[i].Producto.UnidadVenta;
                detalles[i].Total = detalles[i].CantidadProducto * detalles[i].Precio;
                detalles[i].Descripcion = detalles[i].Producto.Descripcion;
                detalles[i].ProductoAux = detalles[i].Producto.IdProducto;
            }
            return detalles;
        }

        public static int NuevoIdVenta()
        {
            return VentasDal.NuevoIdVenta()+1;
        }

        public static void InsertarVetna(Venta venta,List<DetalleVenta> detalle)
        {
            //VentasDal.Insert(venta);
        }

        public static int ObtenerEmpleado_idUsuario(int id)
        {
            //return EmpleadoDal.obtenerId_idUsuario(id);
            return 0;
        }
    }
}
