﻿using System;
using System.Data.SqlClient;
using Comun;
using DAL;

namespace BRL
{
    public class EmpleadoKeyValueListBrl
    {
        public static EmpleadoKeyValueList Obtener(string apellido)
        {
            Operaciones.WriteLogsDebug("EmpleadoKeyValueListBrl", "Obtener", string.Format("{0} Info: {1}",
             DateTime.Now.ToString(),
             "Empezando a ejecutar el método lógica de negocio para Obtener un EmpleadoKeyValueList"));

            try
            {
                return EmpleadoKeyValueListDal.Obtener(apellido);
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("EmpleadoKeyValueListBrl", "Obtener", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("EmpleadoKeyValueListBrl", "Obtener", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
        }
    }
}
