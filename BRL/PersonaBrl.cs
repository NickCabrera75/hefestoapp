﻿using System;
using System.Data.SqlClient;
using Comun;
using DAL;
using System.Data;


namespace BRL
{
    /// <summary>
    /// Clase para manejar la lógica de negocio de la persona
    /// </summary>
    public class PersonaBrl : AbstractBRL
    {
        #region Atributos, Contructores y Propiedades
        //private Persona per;
        private PersonaDal dal;

        public PersonaBrl()
        {

        }
        public PersonaBrl(Persona per)
        {
            //this.per = per;
            dal = new PersonaDal(per);
        }

        public PersonaDal Dal
        {
            get
            {
                return dal;
            }

            set
            {
                dal = value;
            }
        }
        #endregion

        #region Metodos de la clase
        public override void Delete()
        {
            dal.Delete();
        }

        public override void Insert()
        {
            dal.Insert();
        }

        public override DataTable Select()
        {
            dal = new PersonaDal();
            return dal.Select();
        }

        public override void Update()
        {
            dal.Update();
        }
        public Persona Get(int idPersona)
        {
            dal = new PersonaDal();
            return dal.Get(idPersona);
        }
        #endregion

    }
}
